import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxCountdownTimerComponent } from './ngx-countdown-timer.component';

describe('NgxCountdownTimerComponent', () => {
  let component: NgxCountdownTimerComponent;
  let fixture: ComponentFixture<NgxCountdownTimerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NgxCountdownTimerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NgxCountdownTimerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
