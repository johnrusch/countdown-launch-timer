import { Component, OnInit, ViewChild } from '@angular/core';
import { CountdownComponent } from 'ngx-countdown';

@Component({
  selector: 'app-ngx-countdown-timer',
  templateUrl: './ngx-countdown-timer.component.html',
  styleUrls: ['./ngx-countdown-timer.component.scss']
})
export class NgxCountdownTimerComponent implements OnInit {

  constructor() { }

  public launchTime = new Date().setHours(14,58,0);
  public currentTime = new Date().getTime();
  public complete = false;

  public config = {
    leftTime: (this.launchTime - this.currentTime)/1000 // convert miliseconds to seconds
  }

  timerComplete = () => {
    this.complete =  true;
  }
  test = () => {
    this.launchTime = new Date().getTime() + 5000;
  }

  handleEvent = (e: any) => {
    console.log(e);
    if (e.action === 'done'){
      this.timerComplete();
    }
  }

  ngOnInit(): void {
  }

}
